import { register_schema } from "./register";
import { user_schema } from "./user";

const validator = {
    user_schema,
    register_schema
}

const validate =
    (schema: any) => async (req: any, res: any, next: () => any) => {
        try {
            await schema.validate(
                {
                    body: req.body,
                    query: req.query,
                    params: req.params,
                },
                { abortEarly: false }
            )
            return next()
        } catch (err:
            | { name: string; message: string; errors: string[] }
            | any) {
            return res
                .send({
                    name: err.name,
                    message: err.message,
                    errors: err.errors,
                })
                .status(400)
        }
    }
export { validate, validator }
