import * as yup from 'yup'

export const user_schema = yup.object({
  body: yup.object({
    name: yup
      .string()
      .min(3, 'Name too short')
      .max(32, 'Name too long')
      .required('Name is required'),
    password: yup
      .string()
      .min(5, 'Password too short')
      .max(32, 'Password too long')
      .required('Password is required'),
  }),
})
