import jwt from 'jsonwebtoken'

const validate_token = async (
  request: any,
  response: any,
  next: () => void
) => {
  const access = request.get("Authorization")

  if (!access) return response.sendStatus(401)

  const token = access.split(' ')[1]
  
  if (access) {
    jwt.verify(
      token,
      process.env.ACCESS_TOKEN_SECRET ?? '',
      { ignoreExpiration: true },
      async (err: any, res: any) => {
        if (err) return response.sendStatus(403)
        if (res.exp < +new Date() / 1000) {
          return response.sendStatus(403)
        } else {
          request.is_admin = res.is_admin
          request.id = res.id
          next()
        }
      }
    )
  }
}

const validate_token_soft = async (
  request: any,
  _response: any,
  next: () => void
) => {
  const access = request.get("Authorization").split(' ')[1] || null


  if (access) {
    jwt.verify(
      access,
      process.env.ACCESS_TOKEN_SECRET ?? '',
      { ignoreExpiration: true },
      async (err: any, res: any) => {
        if (err) {
          request.is_admin = false

        }
        if (res.exp < +new Date() / 1000) {
          request.is_admin = false

        }
        request.is_admin = res.is_admin
        request.id = res.id

      }
    )
  } else {
    request.is_admin = false

  }
  next()
}

export { validate_token, validate_token_soft }
