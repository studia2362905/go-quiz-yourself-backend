const validate_admin = async (
    request: any,
    response: any,
    next: () => void
) => {
    const is_admin = request.is_admin || null
    if (!is_admin) response.sendStatus(401)
    else next()
}

export { validate_admin }
