import express from 'express'
const router = express.Router()

import { router as quiz } from './quiz'
import { router as user } from './user'

router.get(
  '/',
  async (_request: any, response: any) => {
      try {
          return response.json({message: "All working!"}).status(200)
      } catch (error) {
          console.log(error)
          response.sendStatus(500)
      }
  }
)

router.use("/quiz", quiz)
router.use("/user", user)

export { router }