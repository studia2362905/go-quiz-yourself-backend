import express from 'express'
import { prisma } from '../../utils'
import { validate_token_soft } from '../../middleware'
import { selector } from '../../selectors'

const router = express.Router()

router.get('/me', validate_token_soft, async (request: any, response: any) => {
  try {
    const id = request.id
    if (!id) {
      return response.sendStatus(404)
    }
    const user = await prisma.user.findUnique({ where: { id: id }, select: selector.USER_ME })

    if(!user) {
      return response.sendStatus(404)
    }

    let total = 0

    user.answers.map(ans => {
      total+= ans.points
      })
  
      return response.json({ id: user.id, name: user.name, points: total}).status(200)

  } catch (error) {
    console.log(error)
    response.sendStatus(500)
  }
})
export { router }
