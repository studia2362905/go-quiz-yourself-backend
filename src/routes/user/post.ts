import express from 'express'
import { validator, validate, validate_token } from '../../middleware'
import { selector } from '../../selectors'
import bcrypt from 'bcrypt'
import { prisma } from '../../utils'
import { hash } from '../../utils/hash'
import { generate_token } from '../../utils/token'

const router = express.Router()

router.post(
  '/register',
  validate(validator.register_schema),
  hash,
  async (request: any, response: any) => {
    try {
      const created = await prisma.user.create({
        select: selector.NEW_USER,
        data: {
          name: request.body.name,
          hashed_password: request.body.password,
        },
      })
      return response.json(created).status(200)
    } catch (err) {
      console.log(err)
      response.sendStatus(500)
    }
  }
)

router.post(
  '/login',
  validate(validator.user_schema),
  async (
    request: {
      body: { name: string; password: string }
    },
    response: any
  ) => {
    try {
      const user = await prisma.user.findUnique({
        where: {
          name: request.body.name,
        },
        select: {
          id: true,
          name: true,
          hashed_password: true,
          is_admin: true,
        },
      })
      if (!user) {
        return response.sendStatus(404)
      }

      const password_match = await bcrypt.compare(
        request.body.password,
        user.hashed_password
      )
      if (!password_match) {
        return response.sendStatus(401)
      }

      const token = generate_token({
        id: user.id,
        name: user.name,
        is_admin: user.is_admin,
      })
      /*
      response.cookie('access_token', token, {
        httpOnly: true,
        sameSite: 'none',
        secure: true,
        expires: new Date(Date.now() + 1209600000),
      })*/
      return response
        .send({ id: user.id, name: user.name, access: token })
        .status(200)
    } catch (err) {
      console.log(err)
      response.sendStatus(500)
    }
  }
)

export { router }
