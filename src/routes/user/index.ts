import express from 'express'

import { router as get } from './get'
import { router as post } from './post'
import { router as patch } from './patch'

const router = express.Router()

router.use('/', get)
router.use('/', post)
router.use('/', patch)

export { router }
