import express from 'express'
import { prisma } from '../../utils'
import { selector } from '../../selectors'

const router = express.Router()

router.patch('/:id', async (request: any, response: any) => {
  try {
    const patched = await prisma.quiz.update({
      where: { id: parseInt(request.params.id) },
      data: { is_global: true },
    })
    return response.send(patched).status(200)
  } catch (error) {
    console.log(error)
    response.sendStatus(500)
  }
})

export { router }
