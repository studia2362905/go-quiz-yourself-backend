import express from 'express'
import { prisma } from '../../utils'

const router = express.Router()

router.delete('/all', async (_request: any, response: any) => {
    try {
        const deletedChoices = await prisma.choices.deleteMany({})
        const deletedQuestions = await prisma.question.deleteMany({})
        const deletedQuizes = await prisma.quiz.deleteMany({})
        return response.json({deletedQuizes, deletedQuestions, deletedChoices}).status(200)
    } catch (error) {
        console.log(error)
        response.sendStatus(500)
    }
})

router.delete('/:id', async (request: any, response: any) => {
    try {
            const quiz = await prisma.quiz.delete({
            where: {
                id: parseInt(request.params.id),
            },
    })
        return response.json(quiz).status(200)
    } catch (error) {
        console.log(error)
        response.sendStatus(500)
    }
})

export { router }
