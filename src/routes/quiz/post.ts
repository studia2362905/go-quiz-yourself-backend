import express, { text } from 'express'
import { prisma } from '../../utils'
import { validate_token_soft } from '../../middleware'
import { selector } from '../../selectors'

const router = express.Router()

router.post('/', async (request: any, response: any) => {
  try {
    console.log(request)
    const created = await prisma.quiz.create({
      data: {
        name: request.body.name,
      },
    })

    return response.json(created).status(200)
  } catch (error) {
    console.log(error)
    response.sendStatus(500)
  }
})

router.post('/:id', async (request: any, response: any) => {
  try {
    const created = await prisma.question.create({
      data: {
        quiz_id: parseInt(request.params.id),
        text: request.body.text,
        choices: {
          createMany: { data: request.body.choices },
        },
      },
    })
    return response.json(created).status(200)
  } catch (error) {
    console.log(error)
    response.sendStatus(500)
  }
})

router.post(
  '/check/:id',
  validate_token_soft,
  async (request: any, response: any) => {
    try {
      let correct = 0
      const givenAnswears = request.body
      const quiz = await prisma.quiz.findUnique({
        select: {
          id: false,
          name: false,
          questions: {
            select: {
              id: true,
              text: false,
              choices: {
                select: {
                  id: true,
                  question_id: true,
                  text: false,
                  is_correct: true,
                },
              },
            },
          },
          answers: false,
        },
        where: {
          id: parseInt(request.params.id),
        },
      })
      if (!quiz) return response.sendStatus(500)
      const max = quiz.questions.length * 10
      givenAnswears.map((ans: { question_id: number; answer_id: number }) => {
        const q = quiz.questions.find((e) => e.id === ans.question_id)
        if (q) {
          if (q.choices[0].is_correct) {
            if (ans.answer_id === q.choices[0].id) {
              correct += 10
            }
          }
          if (q.choices[1].is_correct) {
            if (ans.answer_id === q.choices[1].id) {
              correct += 10
            }
          }
          if (q.choices[2].is_correct) {
            if (ans.answer_id === q.choices[2].id) {
              correct += 10
            }
          }
          if (q.choices[3].is_correct) {
            if (ans.answer_id === q.choices[3].id) {
              correct += 10
            }
          }
        }
      })

      if (request.id) {
        const user = await prisma.user.findUnique({
          where: { id: request.id },
          select: selector.USER_ME,
        })
        if (user) {
          const ans = user.answers.find(
            (ans) => ans.quiz_id === parseInt(request.params.id)
          )
          if (!ans) {
            const newans = await prisma.answer.create({
              data: {
                user_id: user.id,
                quiz_id: parseInt(request.params.id),
                points: correct,
              },
            })
            return response
              .json({ score: correct, max: max, newans })
              .status(200)
          } else {
            if (ans.points < correct) {
              const patched = await prisma.answer.updateMany({
                where: {
                  user_id: user.id,
                  quiz_id: parseInt(request.params.id),
                },
                data: {
                  points: correct,
                },
              })
              return response
                .json({ score: correct, max: max, patched })
                .status(200)
            }
          }
        }
      }

      return response.json({ score: correct, max: max }).status(200)
    } catch (error) {
      console.log(error)
      response.sendStatus(500)
    }
  }
)

export { router }
