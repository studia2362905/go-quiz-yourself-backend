import express from 'express'
import { prisma } from '../../utils'
import { validate_token_soft } from '../../middleware'

const router = express.Router()

router.get('/', validate_token_soft, async (request: any, response: any) => {
  try {
    const is_admin = request.is_admin
    const allQuizes = await prisma.quiz.findMany({
      select: {
        id: true,
        name: true,
        is_global: true,
        _count: {
          select: {
            questions: true,
          },
        },
        questions: false,
        answers: false,
      },
      where: {
        ...(is_admin ? {} : { is_global: true }),
      },
    })

    return response.json(allQuizes).status(200)
  } catch (error) {
    console.log(error)
    response.sendStatus(500)
  }
})

router.get('/:id', async (request: any, response: any) => {
  try {
    const quiz = await prisma.quiz.findUnique({
      select: {
        id: true,
        name: true,
        is_global: true,
        questions: {
          select: {
            id: true,
            text: true,
            choices: {
              select: {
                id: true,
                question_id: true,
                text: true,
                is_correct: false,
              },
            },
          },
        },
        answers: false,
      },
      where: {
        id: parseInt(request.params.id),
      },
    })
    return response.json(quiz).status(200)
  } catch (error) {
    console.log(error)
    response.sendStatus(500)
  }
})

export { router }
