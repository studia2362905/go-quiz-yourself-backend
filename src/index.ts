import bodyParser from 'body-parser'
import express from 'express'
import dotenv from 'dotenv'
import cors from 'cors'

dotenv.config()

import { router } from './routes'

const PORT = process.env.PORT || 3001

const app = express()


app.use(
  cors({
    origin: 'http://localhost:3000',
  })
)

app.use(bodyParser.json())

app.use('/api', router)

app.listen(PORT, () => {
  console.log(`App running on port ${PORT}`)
})
