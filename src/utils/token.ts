import jwt from 'jsonwebtoken'

const generate_token = (data: {
  id: number
  name: string
  is_admin: Boolean
}) => {
  return jwt.sign(
    {
      id: data.id,
      name: data.name,
      is_admin: data.is_admin,
      type: 'ACCESS',
    },
    process.env.ACCESS_TOKEN_SECRET!,
    {
      expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN,
    }
  )
}

export { generate_token }
