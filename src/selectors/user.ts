export const NEW_USER = {
    id: true,
    name: true,
    is_admin: true,
}
export const USER_ME = {
    id: true,
    name: true,
    is_admin: true,
    answers: true,
}