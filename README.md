# Go Quiz Yourself Backend

## What is the use of this repo

Backend for Web application to authorize users, connect to database and calculate scores.

## Prerequisites

### Install Node JS

Refer to https://nodejs.org/en/ to install nodejs

### Install Postgres database

Refer to https://www.postgresql.org/download/windows/ to install local database

## Cloning and Installing the Application in local

Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
npm install
```

## Setting env variables

| Varaible                |                   Description                   |
| ----------------------- | :---------------------------------------------: |
| PORT                    |             Port to run aplication              |
| DATABASE_URL            |               Url of the database               |
| SHADOW_DATABASE_URL     |     Url of secound database for migrations      |
| ACCESS_TOKEN_SECRET     |         Key to check token authenticity         |
| ACCESS_TOKEN_EXPIRES_IN | Amount of time after which access token expires |

## Creating prisma client

In order for prisma to work create prisma client and migrate database

```bash
npx prisma migrate dev --name init
```

## Running the Application

In order to run the application Type the following command

```bash
npm run dev
```

The Application Runs on **localhost:PORT** (4000 or assigned in .env file)

## Used technologies and libraries

1. [Express](https://expressjs.com) Framework with [Typescript](https://www.typescriptlang.org)
2. [Prisma](https://www.prisma.io) to connect to database
3. [dotenv](https://www.npmjs.com/package/dotenv) for injection of env variables
4. [jwt](https://jwt.io/) to work with json web tokens
5. [yup](https://www.npmjs.com/package/yup) for data validation
